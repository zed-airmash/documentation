# Documentation

## Projects

- [ ] Frontend (Mirrored from https://github.com/airmash-refugees/airmash-frontend)
- [ ] Backend (Mirrored from https://github.com/airmash-refugees/airmash-backend)
- [ ] Wight's Frontend (Mirrored from https://github.com/wight-airmash/ab-client)
- [ ] Wight's Backend (Mirrored from https://github.com/wight-airmash/ab-server)
- [ ] Wight's Protocol (Mirrored from https://github.com/wight-airmash/ab-protocol)
- [ ] Steamroller's Backend (Mirrored from https://github.com/steamroller-airmash/airmash-server)

## TODO

- [ ] Compare `airmash-frontend` and `ab-client`
- [ ] Compare `airmash-backend` and `ab-server`
- [ ] Setup local frontend
- [ ] Setup local backend
- [ ] Bots?
- [ ] Review backend
